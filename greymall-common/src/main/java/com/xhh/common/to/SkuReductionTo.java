package com.xhh.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-11 13:55
 **/
@Data
public class SkuReductionTo {
    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}
