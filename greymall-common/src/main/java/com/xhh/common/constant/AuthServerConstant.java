package com.xhh.common.constant;

/**
 * @description 分布式高级篇完成
 * @author: wei-xhh
 * @create: 2020-07-19
 **/
public class AuthServerConstant {
    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";
    public static final String LOGIN_USER = "loginUser";
}
