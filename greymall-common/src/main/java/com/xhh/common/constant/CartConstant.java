package com.xhh.common.constant;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-24
 */
public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    public static final int TEMP_USER_COOKIE_TIMEOUT = 60 * 60 * 24 * 30;
}
