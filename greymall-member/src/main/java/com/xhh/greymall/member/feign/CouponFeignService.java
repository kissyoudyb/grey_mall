package com.xhh.greymall.member.feign;

import com.xhh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-03 09:29
 **/
@FeignClient("greymall-coupon")
public interface CouponFeignService {
    @GetMapping("/coupon/coupon/member/list")
    public R memberCoupon();
}
