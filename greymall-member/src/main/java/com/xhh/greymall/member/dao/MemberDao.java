package com.xhh.greymall.member.dao;

import com.xhh.greymall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:23:10
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
