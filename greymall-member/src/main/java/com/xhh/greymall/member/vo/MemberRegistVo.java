package com.xhh.greymall.member.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @description: 会员的注册vo
 * @author: wei-xhh
 * @create: 2020-07-20
 */
@Data
public class MemberRegistVo {
    private String username;

    private String password;

    private String phone;

    private String code;
}
