package com.xhh.greymall.member.exception;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-20
 */
public class UsernameExistException extends RuntimeException {
    public UsernameExistException() {
        super("用户名存在");
    }
}
