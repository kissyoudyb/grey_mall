package com.xhh.greymall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:23:10
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

