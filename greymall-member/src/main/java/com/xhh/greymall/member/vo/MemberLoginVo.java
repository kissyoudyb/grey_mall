package com.xhh.greymall.member.vo;

import lombok.Data;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-20
 */
@Data
public class MemberLoginVo {
    private String loginacct;
    private String password;
}
