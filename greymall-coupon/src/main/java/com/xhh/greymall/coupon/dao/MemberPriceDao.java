package com.xhh.greymall.coupon.dao;

import com.xhh.greymall.coupon.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品会员价格
 * 
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:10:45
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {
	
}
