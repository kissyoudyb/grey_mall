package com.xhh.greymall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.coupon.entity.HomeAdvEntity;

import java.util.Map;

/**
 * 首页轮播广告
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:10:45
 */
public interface HomeAdvService extends IService<HomeAdvEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

