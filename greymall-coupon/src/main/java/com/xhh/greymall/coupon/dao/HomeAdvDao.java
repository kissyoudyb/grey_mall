package com.xhh.greymall.coupon.dao;

import com.xhh.greymall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:10:45
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
