package com.xhh.greymall.thirdparty;

import com.alibaba.fastjson.JSON;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreymallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreymallThirdPartyApplication.class, args);
    }

}
