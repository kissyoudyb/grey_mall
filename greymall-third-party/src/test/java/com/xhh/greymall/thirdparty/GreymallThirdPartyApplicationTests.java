package com.xhh.greymall.thirdparty;

import com.aliyun.oss.OSS;
import com.xhh.greymall.thirdparty.component.SmsComponent;
import com.xhh.greymall.thirdparty.util.HttpUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class GreymallThirdPartyApplicationTests {

    @Autowired
    OSS ossClient;

    @Autowired
    SmsComponent smsComponent;

    @Test
    public void testSendCode(){
        smsComponent.sendSmsCode("17827434434", "3433");
    }

    @Test
    public void sendSms(){
        String host = "https://smsmsgs.market.alicloudapi.com";  // 【1】请求地址 支持http 和 https 及 WEBSOCKET
        String path = "/sms/";  // 【2】后缀
        String appcode = ""; // 【3】开通服务后 买家中心-查看AppCode
        Map<String,String> map = new HashMap<>();
        map.put("code", "3423");
        map.put("phone", "17827434434");
        map.put("skin", "1");
        map.put("sign", "13");

        String s = HttpUtils.doGet(host, path, appcode, map);
        System.out.println(s);
    }

    @Test
    void testUpload() throws FileNotFoundException {
//        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
//
        // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = new FileInputStream("E:\\important-workspaces\\workspace-shopping\\2.jpg");
        ossClient.putObject("greymall", "3.jpg", inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();
        System.out.println("上传成功..");
    }

    @Test
    void contextLoads() {
    }

}
