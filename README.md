# 灰灰商城
copyright 尚硅谷-谷粒商城2020

技术概览：

- 数据库：mysql,Mybatis-plus
- 简化实体类：lombok
- 自定义校验：javax.validation
- 配置中心：alibaba-nacos-config
- 服务注册发现：alibaba-nacos-discovery
- 前端模版：vue（renren开源模版）
- 容器：docker
- 各服务调用：feign
- 检索：ElasticSearch
- 消息队列：RabbitMQ 
- 缓存：redis
- 异步：CompletableFuture
- 动静分离：nginx
- 网络请求：OKhttp
- 压力测试：apache-jmeter-5.2.1
- 分布式事务：alibaba-seata（试用）
- 熔断降级限流：alibaba-sentinel
- 服务链路追踪+可视化：Sleuth+Zipkin
- 部署：k8s+kubeSphere

### 分布式基础篇
灰灰商城-电商项目管理后台

#### 环境搭建

- linux下安装docker
- 下载mysql,镜像
- 导入sql
- 人人开源后台管理前端，代码生成器

#### 介绍
1. 分布式基础概念
* 微服务
* 注册中心(nacos)
* 配置中心(nacos)
* 远程调用(openfeign)
* 网关(gateway)

2. 开发规范
* 数据校验JSR303,全局异常处理，全局统一返回，全局跨域处理
* 枚举状态，业务状态码，VO与TO与PO划分，逻辑删除
* Lombok: @Data,@Slf4j


### 分布式高级篇
笔记资料参考分布式高级篇笔记


### 集群架构篇
笔记资料参考集群架构篇笔记


感谢尚硅谷！！
