package com.xhh.greymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.product.entity.AttrEntity;
import com.xhh.greymall.product.vo.AttrRespVO;
import com.xhh.greymall.product.vo.AttrVO;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVO attr);

    PageUtils queryBaseOrSaleAttrPage(Map<String, Object> params, Long attrId, String type);

    AttrRespVO getAttrInfo(Long attrId);

    void updateAttr(AttrVO attr);

    /**
     * 在指定的所有属性集合里，跳出检索属性
     * @param attrIds
     * @return
     */
    List<Long> selectSearchAttrs(List<Long> attrIds);
}

