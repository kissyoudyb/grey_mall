package com.xhh.greymall.product.feign;

import com.xhh.common.to.SpuBoundsTo;
import com.xhh.common.to.SkuReductionTo;
import com.xhh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-11 13:01
 **/
@FeignClient("greymall-coupon")
public interface CouponFeignService {

    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundsTo spuBoundsTo);

    @PostMapping("/coupon/skufullreduction/save")
    R saveSkuReduction(@RequestBody SkuReductionTo spuReductionTo);
}
