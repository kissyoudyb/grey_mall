package com.xhh.greymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.product.entity.AttrEntity;
import com.xhh.greymall.product.entity.AttrGroupEntity;
import com.xhh.greymall.product.vo.AttrGroupRelationVO;
import com.xhh.greymall.product.vo.AttrGroupWithAttrsVO;
import com.xhh.greymall.product.vo.SkuItemVo;
import com.xhh.greymall.product.vo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     *  根据catId查询属性分组
     * @param params
     * @param catId
     * @return
     */
    PageUtils queryPage(Map<String, Object> params, Long catId);

    List<AttrEntity> queryAttrRelation(Long attrgroup);

    void deleteRelation(AttrGroupRelationVO[] vos);

    PageUtils queryNoAttrRelation(Long attrgroup, Map<String, Object> params);

    List<AttrGroupWithAttrsVO> getAttrGroupWithAttrsByCatelogId(Long catelogId);

    List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId);
}

