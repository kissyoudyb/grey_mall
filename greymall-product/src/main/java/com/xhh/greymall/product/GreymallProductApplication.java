package com.xhh.greymall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 1.整合Mybatis-Plus
 *   1).导入依赖
 *
 *         <dependency>
 *             <groupId>com.baomidou</groupId>
 *             <artifactId>mybatis-plus-boot-starter</artifactId>
 *             <version>3.3.1</version>
 *         </dependency>
 *
 *    2).配置
 *       1. 配置数据源
 *          1).导入数据库的驱动
 *          2).在application.yml配置数据源相关信息
 *       2.配置MyBatis-Plus
 *          1).使用@MapperScan
 *          2).告诉MyBatis-Plus sql映射文件位置
 *          mybatis-plus:
 *              global-config:
 *                  db-config:
 *                      id-type: auto
 *                  mapper-locations: classpath:...
 *
 * 2.使用逻辑删除
 *   全局
 *    logic-delete-value: 1
 *    logic-not-delete-value: 0
 *
 *   某个字段
 *      @TableLogic(value = "1", delval = "0")
 *
 *
 */

@EnableRedisHttpSession
@EnableFeignClients("com.xhh.greymall.product.feign")
@EnableDiscoveryClient
@MapperScan("com.xhh.greymall.product.dao")
@SpringBootApplication
public class GreymallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreymallProductApplication.class, args);
    }

}
