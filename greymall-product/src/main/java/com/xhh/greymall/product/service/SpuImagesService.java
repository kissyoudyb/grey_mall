package com.xhh.greymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.product.entity.SpuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * spu图片
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuImages(Long spuId, List<String> images);
}

