package com.xhh.greymall.product.vo;

import lombok.Data;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-09 15:34
 **/
@Data
public class AttrGroupRelationVO {
    //attrId: 1, attrGroupId: 1
    private Long attrId;
    private Long attrGroupId;
}
