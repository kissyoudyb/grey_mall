package com.xhh.greymall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-06-25 10:40
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Catalog2VO {

    private String catalog1Id;
    private List<Catalog3> catalog3List;
    private String id;
    private String name;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class Catalog3{
        private String catalog2Id;
        private String id;
        private String name;
    }
}
