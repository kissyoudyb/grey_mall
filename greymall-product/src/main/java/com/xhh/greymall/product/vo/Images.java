/**
  * Copyright 2020 bejson.com 
  */
package com.xhh.greymall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2020-05-10 18:49:41
 *
 * @author
 */
@Data
public class Images {

    private String imgUrl;
    private int defaultImg;

}