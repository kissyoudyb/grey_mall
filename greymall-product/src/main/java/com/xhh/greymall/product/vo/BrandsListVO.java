package com.xhh.greymall.product.vo;

import lombok.Data;

/**
 * @description
 *
 *   "brandId": 0,
 * 	 "brandName": "string",
 *
 * @author: weiXhh
 * @create: 2020-05-10 16:01
 **/
@Data
public class BrandsListVO {
    private Long brandId;
    private String brandName;
}
