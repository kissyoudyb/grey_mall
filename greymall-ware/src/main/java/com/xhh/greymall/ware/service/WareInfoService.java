package com.xhh.greymall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.ware.entity.WareInfoEntity;
import com.xhh.greymall.ware.vo.FareVo;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 仓库信息
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:42:07
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据用户的收货地址计算运费
     * @param attrId
     * @return
     */
    FareVo getFare(Long attrId);
}

