package com.xhh.greymall.ware.dao;

import com.xhh.greymall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:42:07
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
