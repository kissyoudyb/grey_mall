package com.xhh.greymall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @description {purchaseId: 1, items: [1, 2]}
 * @author: weiXhh
 * @create: 2020-05-12 12:03
 **/
@Data
public class MergeVO {

    private Long purchaseId;
    private List<Long> items;
}
