package com.xhh.greymall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreymallTestSsoClient2Application {

    public static void main(String[] args) {
        SpringApplication.run(GreymallTestSsoClient2Application.class, args);
    }

}
