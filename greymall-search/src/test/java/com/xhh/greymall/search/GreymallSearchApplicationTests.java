package com.xhh.greymall.search;


import com.alibaba.fastjson.JSON;
import com.xhh.greymall.search.config.GreymallElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootTest
class GreymallSearchApplicationTests {

    @Autowired
    RestHighLevelClient client;

    @Test
    void contextLoads() {
        System.out.println(client);
    }

    // 测试保存数据到es
    @Test
    void indexData() throws IOException {
        IndexRequest request = new IndexRequest("users");
        request.id("1");
//        request.source("userName","zhangsan", "age", 20, "gender", "男");
        User user = new User();
        user.setUserName("wei-xhh");
        user.setAge(20);
        user.setGender("男");
        String jsonString = JSON.toJSONString(user);
        request.source(jsonString, XContentType.JSON); // 保存的内容

        IndexResponse index = client.index(request, GreymallElasticSearchConfig.COMMON_OPTIONS);

    }
    // 测试查询数据
    @Test
    void searchData() throws IOException {
//        GET /bank/_search
//        {
//            "query": {
//            "term": {
//                "address": {
//                    "value": "mill"
//                }
//            }
//        }
//        }
        // 查询的索引
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("bank");
        // 条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(
                QueryBuilders.termQuery("address", "mill")
        );

        System.out.println(searchSourceBuilder.toString());
        searchRequest.source(searchSourceBuilder);

        // 查询
        SearchResponse search = client.search(searchRequest, GreymallElasticSearchConfig.COMMON_OPTIONS);
        System.out.println(search.toString());
    }
    // 测试查询数据
    @Test
    void searchData2() throws IOException {
//        GET /bank/_search
//        {
//            "query": {
//            "term": {
//                "address": {
//                    "value": "mill"
//                }
//            }
//        },
//            "aggs": {
//            "aggAvg": {
//                "avg": {
//                    "field":"age"
//                }
//            },
//            "balanceAgg":{
//                "terms": {
//                    "field": "balance",
//                            "size": 10
//                }
//            }
//        }
//        }
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("bank");

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.termQuery("address", "mill"));

        // 按照薪资的值进行分布
        TermsAggregationBuilder balance = AggregationBuilders.terms("balanceAgg")
                .field("balance");
        searchSourceBuilder.aggregation(balance);

        // 计算平均年龄
        AvgAggregationBuilder age = AggregationBuilders.avg("ageAvg").field("age");
        searchSourceBuilder.aggregation(age);

        searchRequest.source(searchSourceBuilder);
        System.out.println(searchSourceBuilder.toString());

        SearchResponse search = client.search(searchRequest, GreymallElasticSearchConfig.COMMON_OPTIONS);

        System.out.println(search.toString());

        // 封装结果
        SearchHits hits = search.getHits();
        SearchHit[] data = hits.getHits();
        for (SearchHit datum : data) {
            String sourceAsString = datum.getSourceAsString();
            ResultData resultData = JSON.parseObject(sourceAsString, ResultData.class);
            System.out.println(resultData);
        }

        Aggregations aggregations = search.getAggregations();
//        List<Aggregation> aggregationsData = aggregations.asList();
//        for (Aggregation aggregationsDatum : aggregationsData) {
//            String name = aggregationsDatum.getName();
//            System.out.println(name);
//        }
        Terms balanceAgg = aggregations.get("balanceAgg");
        for (Terms.Bucket bucket : balanceAgg.getBuckets()) {
            String keyAsString = bucket.getKeyAsString();
            System.out.println("薪资" + keyAsString);
        }

        Avg aggAvg = aggregations.get("ageAvg");
        double value = aggAvg.getValue();
        System.out.println("平均年龄" + value);

    }

    @Data
    class User{
        private String userName;
        private Integer age;
        private String gender;
    }

    @ToString
    @Data
    static class ResultData{
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

}
