package com.xhh.greymall.search.service;

import com.xhh.greymall.search.vo.SearchParam;
import com.xhh.greymall.search.vo.SearchResult;

/**
 * @description
 * @author: wei-xhh
 * @create: 2020-07-12
 **/
public interface MallSearchService {
    /**
     *
     * @param param 检索的所有参数
     * @return 返回检索的结果，包含页码需要的所有信息
     */
    SearchResult search(SearchParam param);
}
