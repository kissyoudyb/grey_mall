package com.xhh.greymall.seckill.service;

import com.xhh.greymall.seckill.to.SeckillSkuRedisTo;

import java.util.List;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-08-01
 */
public interface SeckillService {
    void uploadSeckillSkuLatest3Days();

    /**
     * 返回当前时间可以参与的秒杀商品信息
     * @return
     */
    List<SeckillSkuRedisTo> getCurrentSeckillSkus();

    SeckillSkuRedisTo getSkuSeckillInfo(Long skuId);

    String kill(String killId, String key, Integer num);
}
