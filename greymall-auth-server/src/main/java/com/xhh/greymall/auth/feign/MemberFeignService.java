package com.xhh.greymall.auth.feign;

import com.xhh.common.utils.R;
import com.xhh.greymall.auth.vo.SocialUser;
import com.xhh.greymall.auth.vo.UserLoginVo;
import com.xhh.greymall.auth.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-20
 */
@FeignClient("greymall-member")
public interface MemberFeignService {
    @PostMapping("/member/member/regist")
    R regist(@RequestBody UserRegistVo vo);

    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping("/member/member/oauth2/login")
    R oauthLogin(@RequestBody SocialUser socialUser);
}
