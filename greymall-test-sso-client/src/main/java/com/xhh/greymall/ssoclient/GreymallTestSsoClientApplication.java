package com.xhh.greymall.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreymallTestSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreymallTestSsoClientApplication.class, args);
    }

}
