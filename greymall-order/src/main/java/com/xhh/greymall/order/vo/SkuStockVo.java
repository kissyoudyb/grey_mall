package com.xhh.greymall.order.vo;

import lombok.Data;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-27
 */
@Data
public class SkuStockVo {
    private Long skuId;
    private Boolean hasStock;
}
