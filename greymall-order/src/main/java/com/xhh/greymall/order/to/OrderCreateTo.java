package com.xhh.greymall.order.to;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.xhh.greymall.order.entity.OrderEntity;
import com.xhh.greymall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-27
 */
@Data
public class OrderCreateTo {
    private OrderEntity order;
    private List<OrderItemEntity> orderItems;
    private BigDecimal payPrice; //订单计算的应付价格
    private BigDecimal fare; //运费
}
