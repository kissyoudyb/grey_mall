package com.xhh.greymall.order.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * 订单确认页需要的数据
 * @author: wei-xhh
 * @create: 2020-07-26
 */
//@Data
public class OrderConfirmVo {
    //收货地址
    @Setter @Getter
    List<MemberAddressVo> address;

    //所有选中的购物项
    @Setter @Getter
    List<OrderItemVo> items;

    //发票记录...

    //优惠卷信息
    @Setter @Getter
    Integer integration;

    //防重令牌
    @Setter @Getter
    String orderToken;

    @Setter @Getter
    Map<Long,Boolean> stocks;
    //订单总额
//    BigDecimal total;

    public Integer getCount(){
        Integer i = 0;
        if(items != null){
            for (OrderItemVo item : items) {
                i+=item.getCount();
            }
        }
        return i;
    }

    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal("0");
        if(items != null){
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));

                sum = sum.add(multiply);
            }
        }

        return sum;
    }

    //应付价格
//    BigDecimal payPrice;

    public BigDecimal getPayPrice() {
        return getTotal();
    }


}
