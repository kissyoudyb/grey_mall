package com.xhh.greymall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.xhh.greymall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private String app_id = "2016102600761204";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCUuVxyLDs+nKjOVrUJnQhwpVYzD7milbGrA4w/T18YkpusRX6BHJKcpHPSfkAw7NgMk9guCyKeW3WbfwRMGgtijQNXzDOYdBlCoOkom2tPUI4FaTuSsHvxWwdIYKfg5A33JPQsRy63iRsR33tvvRm9v0WN14ARZxHZ/6sS9Lo2wFwBdY/vIcMFifV+u0uCIZXGOC5PJxVmdvYOf5Xq87032ahMjz5eLcn5vAPOq+HgBqkJhWF8xjzL7v/ZQrm9KsjDwAeY1gCMnuDKSfegD22+/bD2iEG+cD9Mh/Ia5AAMeXfW+MwqYa3WhS80DD8oi5+NOe+Xb82K2LKWnMfxGnXZAgMBAAECggEAfw+qKqUdbI29LqQngXIW/t5SuccqivwnUXrzfDdcgIro0xKAVOBLZeaeSWyxfNssbfp1zEiZJ//th47J5hauOWFORNliD1LJLgJ5kMbPtHCXFYplahq7mWVy4GQrqVhNkiORNK8P+5/4b5mmRL2jW1gpnVhKwxNyzfO/fa911EXN+NuMViiZs9BCJOd4NrVvWeczEs+o7CgxgEYAT6s9QtEuDuHeBuBxCF3iNFruM34Av6qclwVDfK/cAFhyjZfwESXo2sE62usV1aT4bHgPBca4KBVVxUSc9Y3U8EMQcmpNt9YCb6GtV2v7f0ZSJ7twNGyDPM/vACvWgK+VeBVX/QKBgQDa3e236Ws9oFK1KLmu1Q4+uH2xXtYgj855lQO/1BUPcBauauwZO/9NdrVBWrfaIQ13I0CD6nJ2j0hbKNKi3GKarzp8IMEoGx7W5oebj/kAfd85RVabW3pcL88fcP8xk0Ce2e15m01T1Ik/yT1Nm/8/lu08oi5+ytqoGBvn3R3hgwKBgQCt9OkVbrCshaV309tbLv/vfhm/yc0yb37zHruvEm6sw9m0c+tfNwKd6+cvqZyNlu3yQH1nWdDjqLU3WqhN+PGPRANIqq5haTYKD2IUJTDE8wANx2YQ3pcM4M4CEBYQVCpzAqH113X9ggMqB/tdXpmWtItGdYfrxEgiZ3ftR4i4cwKBgBm6bO9pqQmdhxt0ph8/PGVzGpN3KoS5YvQ36DDspW0jKAdo2kg9HU3l3njISUSDDiorIrJfJQV64m6U5e3vKkg0wRNGZm5ogb+Rae+HhSXhphP9aqjooAGmPRrdFaKmRLpns9rPvs9T9K5i4an15JN9EnZGOmD9R6bq7pzbCWAxAoGAYa9AHFr2N0staia77W5tJk3Ho91srz/RKDE/78YIBFhOrnjR2bwIaioIvskeMpxorjrssNbM6KraaGUuUerTq0HkHjBKkEUGpYuR5gIkatSSWSp59Esqw36R8LKb0TuSQtddn7IIff0+TaIB06LF2M010LE7THgCkd9MhyA+dGUCgYA2YIvlC08skHxZhjYwOE0EH4iu1Ed6SxNaBRm3UUryE+lOsDT0IWo6B41lRMNdksYjVi6ZLxp7DGOwvc5CJAcaIx3CG09rpiGkiFP+EZ1eHhhUZTopf8kLayu0DA5aqp9BKFG6uI0pXVEu97+FE9dpIY9Zv5zX4EUilqs+7GlRAA==";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuUFTNrG55mGuWC/YBd+ZlnCowfBf+bEesLBiAUJ4BeLDVgkEHkhAA+PdKKKfw/1CBnX581Uq2MPv7roHIF0q9UtVNkMACG8R+FiRXN96gNB4+4bAb09D1g6/Da0JGVBlF9O4wlzzGDGwNnbiqEiPJDzC4nYLIGpXB8wPjnd9dETdwPh4ZqHlW/QbT5a5FGADYJ++lcPH3Bty6VAkrb5FKO7+9EWxcD1mtm+vvo5hXjNXiLstFKyheJDE3lC7Gen6bj2dEo4+JOgC8tXJciYxHUy3lu4U2/id3v7laaGAXBBhg63lmHk6ienP19ThhyJwh7MPPRqx/ZH2heehyI9O5wIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private String notify_url = "http://weixhh.utools.club/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private String return_url = "http://member.greymall.com/memberOrder.html";

    // 签名方式
    private String sign_type = "RSA2";

    // 字符编码格式
    private String charset = "utf-8";

    private String timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"timeout_express\":\""+ timeout +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应：" + result);

        return result;

    }
}
