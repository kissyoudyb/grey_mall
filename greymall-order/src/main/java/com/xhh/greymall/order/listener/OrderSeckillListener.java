package com.xhh.greymall.order.listener;

import com.rabbitmq.client.Channel;
import com.xhh.common.to.mq.SeckillOrderTo;
import com.xhh.greymall.order.entity.OrderEntity;
import com.xhh.greymall.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-08-02
 */
@Slf4j
@RabbitListener(queues = "order.seckill.order.queue")
@Component
public class OrderSeckillListener {

    @Autowired
    OrderService orderService;

    @RabbitHandler
    public void listener(SeckillOrderTo seckillOrder, Channel channel, Message message) throws IOException {

        try {
            log.info("准备创建秒杀单的详细信息。。");
            orderService.createSeckillOrder(seckillOrder);
            //手动接消息
            //手动调用支付宝收单

            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }

}
