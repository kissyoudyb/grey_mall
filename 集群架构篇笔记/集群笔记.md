[TOC]

* * *

# 灰灰商城-分布式高级篇-15

## 集群

### Mysql集群

#### 集群方案

- Mysql-MMM（mysql主主复制管理器）
- MHA（Mysql高可用方面是一个相对成熟的方案）
- InnoDB Cluster（支持自动Failover，强一致性，读写分离，读库高可用，读请求负载均衡，推荐方案）


#### Docker安装模拟Mysql主从复制集群

**记得关闭防火墙**

##### 1、创建Master实例并启动

```
docker run -p 3307:3306 --name mysql-master \
-v /mydata/mysql/master/log:/var/log/mysql \
-v /mydata/mysql/master/data:/var/lib/mysql \
-v /mydata/mysql/master/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-d mysql:5.7
```

参数说明：
-p 3307:3306:将容器的3306映射到主机的3307端口
-v 挂载
-e 初始化root用户密码

* * *
修改master基本配置

vim /mydata/mysql/master/conf/my.cnf

```
[client]
default-character-set=utf8

[mysql]
default-character-set=utf8

[mysqld]
init_connect='SET collation_connection=uft8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve
```

注意：skip-name-resolve 一定要加，不然连接mysql会超级慢
* * *
添加master主从复制部分配置

```
server_id=1
log-bin=mysql-bin
read-only=0
binlog-do-db=greymall_ums
binlog-do-db=greymall_pms
binlog-do-db=greymall_oms
binlog-do-db=greymall_sms
binlog-do-db=greymall_wms
binlog-do-db=greymall_admin

replicate-ignore-db=mysql
replicate-ignore-db=sys
replicate-ignore-db=infomation_schema
replicate-ignore-db=performance_schema
```

重启master

##### 2、创建Slave实例并启动

```
docker run -p 3317:3306 --name mysql-slaver-01 \
-v /mydata/mysql/slaver/log:/var/log/mysql \
-v /mydata/mysql/slaver/data:/var/lib/mysql \
-v /mydata/mysql/slaver/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-d mysql:5.7
```

* * *
修改slaver基本配置

vim /mydata/mysql/slaver/conf/my.cnf

```
[client]
default-character-set=utf8

[mysql]
default-character-set=utf8

[mysqld]
init_connect='SET collation_connection=uft8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve
```

* * *
添加master主从复制部分配置

```
server_id=2
log-bin=mysql-bin
read-only=1
binlog-do-db=greymall_ums
binlog-do-db=greymall_pms
binlog-do-db=greymall_oms
binlog-do-db=greymall_sms
binlog-do-db=greymall_wms
binlog-do-db=greymall_admin

replicate-ignore-db=mysql
replicate-ignore-db=sys
replicate-ignore-db=infomation_schema
replicate-ignore-db=performance_schema
```

##### 3、为master授权用户来他的同步数据

1、进入master容器
docker exec -it mysql /bin/bash

2、进入mysql内部（mysql -uroot -p）

- 授权root可远程访问（主从无关，为了方便我们远程连接mysql）
grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option; flush privileges;

- 添加用来同步的用户

GRANT REPLICATION SLAVE ON *.*  TO 'backup'@'%' IDENTIFIED BY '123456';

3、查看master状态

show master status



##### 4、配置slaver同步master数据

1、进入slaver容器

docker exec -it mysql-slaver-01 /bin/bash

2、进入mysql内部（mysql -uroot -p）

- 授权root可以远程访问（主从无关，为了方便我们远程连接mysql）
grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option; flush privileges;

- 设置主库连接
change master to master_host='192.168.80.133',master_user='backup',master_password='123456',master_log_file='mysql-bin.000001',master_log_pos=0,master_port=3307;

- 启动从库同步
start slave;

- 查看从库状态
show slave status

#### 使用shardingsphere

文档：

[https://shardingsphere.apache.org/document/legacy/4.x/document/cn/overview/](https://shardingsphere.apache.org/document/legacy/4.x/document/cn/overview/)


### Mysql集群

#### 使用redis-cluster

##### 1、创建6个redis节点

```sh
for port in $(seq 7001 7006); \
do \
mkdir -p /mydata/redis/node-${port}/conf
touch /mydata/redis/node-${port}/conf/redis.conf
cat <<EOF>/mydata/redis/node-${port}/conf/redis.conf
port ${port}
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 192.168.80.133
cluster-announce-port ${port}
cluster-announce-bus-port 1${port}
appendonly yes
EOF
docker run -p ${port}:${port} -p 1${port}:1${port} --name redis-${port} \
-v /mydata/redis/node-${port}/data:/data \
-v /mydata/redis/node-${port}/conf/redis.conf:/etc/redis/redis.conf \
-d redis:5.0.7 redis-server /etc/redis/redis.conf; \
done
```
docker stop ${docker ps -a |grep redis-700 | awk '{print $1}}'}
docker rm $(docker ps -a |grep redis-700 | awk '{print $1}')

##### 2、使用redis建立集群

```
docker exec -it redis-7001 bash

redis-cli --cluster create 192.168.80.133:7001 192.168.80.133:7002 192.168.80.133:7003 192.168.80.133:7004 192.168.80.133:7005 192.168.80.133:7006 --cluster-replicas 1
```

### Elasticsearch集群

#### 集群搭建

所有之前先运行: sysctl -w vm.max_map_count=262144（防止jvm报错）
只是测试，所以临时修改，永久修改使用下面

echo vm.max_map_count=262144 >> /etc/sysctl.conf
sysctl -p

##### 1、准备docker网络

查看
docker network ls

创建
docker network create --driver bridge --subnet=172.18.12.0/16 --gateway=172.18.1.1 mynet

查看网络信息
docker network inspect mynet

以后使用--network=mynet --ip 172.18.12.x 指定ip

##### 2、3-Master节点创建

```
for port in $(seq 1 3); \
do \
mkdir -p /mydata/elasticsearch/master-${port}/config
mkdir -p /mydata/elasticsearch/master-${port}/data
chmod -R 777 /mydata/elasticsearch/master-${port}
cat << EOF >/mydata/elasticsearch/master-${port}/config/elasticsearch.yml
cluster.name:my-es #集群的名称，同一个集群该值必须设置成相同的
node.name:es-master-${port} #该节点的名字
node.master:true #该节点有机会成为master节点
node.data:false #该节点可以存储数据
network.host:0.0.0.0
http.host:0.0.0.0 #所有http均可访问
http.port:920${port}
transport.tcp.port:930${port}
discovery.zen.ping_timeout:10s #设置集群中自动发现其他节点时ping连接的超时时间
discovery.seed_hosts:['172.18.12.21:9301','172.18.12.22:9302','172.18.12.23:9303'] #设置集群中的Master节点的初始列表，可以通过这些节点来自动发现其他新加入集群的节点，es7的新增配置
cluster.initial_master_nodes:['172.18.12.21'] #新集群初始时的候选主节点，es7的新增配置
EOF
docker run --name elasticsearch-node-${port} \
-p 920${port}:920${port} -p 930${port}:930${port} \
--netword=mynet --ip 172.18.12.2${port} \
-e ES_JAVA_OPTS="-Xms300m -Xmx300m" \
-v /mydata/elasticsearch/master-${port}/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /mydata/elasticsearch/master-${port}/data:/usr/share/elasticsearch/data \
-v /mydata/elasticsearch/master-${port}/plugins:/usr/share/elasticsearch/plugins \
-d elasticsearch:7.4.2
done
```

##### 3、3-Node节点创建


```
for port in $(seq 4 6); \
do \
mkdir -p /mydata/elasticsearch/master-${port}/config
mkdir -p /mydata/elasticsearch/master-${port}/data
chmod -R 777 /mydata/elasticsearch/master-${port}
cat << EOF >/mydata/elasticsearch/master-${port}/config/elasticsearch.yml
cluster.name:my-es #集群的名称，同一个集群该值必须设置成相同的
node.name:es-node-${port} #该节点的名字
node.master:false #该节点有机会成为master节点
node.data:true #该节点可以存储数据
network.host:0.0.0.0
http.host:0.0.0.0 #所有http均可访问
http.port:920${port}
transport.tcp.port:930${port}
discovery.zen.ping_timeout:10s #设置集群中自动发现其他节点时ping连接的超时时间
discovery.seed_hosts:['172.18.12.21:9301','172.18.12.22:9302','172.18.12.23:9303'] #设置集群中的Master节点的初始列表，可以通过这些节点来自动发现其他新加入集群的节点，es7的新增配置
cluster.initial_master_nodes:['172.18.12.21'] #新集群初始时的候选主节点，es7的新增配置
EOF
docker run --name elasticsearch-node-${port} \
-p 920${port}:920${port} -p 930${port}:930${port} \
--netword=mynet --ip 172.18.12.2${port} \
-e ES_JAVA_OPTS="-Xms300m -Xmx300m" \
-v /mydata/elasticsearch/master-${port}/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /mydata/elasticsearch/master-${port}/data:/usr/share/elasticsearch/data \
-v /mydata/elasticsearch/master-${port}/plugins:/usr/share/elasticsearch/plugins \
-d elasticsearch:7.4.2
done
```